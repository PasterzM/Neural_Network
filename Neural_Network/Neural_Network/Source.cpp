#include <iostream>
#include "Network.h"

using namespace std;

void main() {
	cout << "Start program" << endl;

	vector<unsigned> top;

	top.push_back(3);
	top.push_back(2);
	top.push_back(1);

	Network net(top , true);

	cout << "End program" << endl;
	top.clear();
	getchar();
}