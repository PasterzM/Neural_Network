#pragma once
#include <vector>
#include <list>
#include "Neuron.h"

typedef std::vector<Neuron> Layer;

class Network {
	
public:
	Network();
	Network(const std::vector<unsigned>& netTopology,bool isShowingWork = false);
	void SaveNeuronsValues();
	
	bool AddLayer(int numberOfNeutrons);
	
	void Learn(const std::vector<double>& vInput);
	void BackwardPropagation(const std::vector<double>& targetVal);	//wsteczna propagacja
	void GetResults(std::vector<double>& res) const;

	Layer& operator[](unsigned it);
private:
	bool isNetworkCreate;
	std::vector<Layer> layers;
};