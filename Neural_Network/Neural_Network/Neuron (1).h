#pragma once
#include <vector>

struct Connection {
	double dtWeight;
	double weight;
};

class Neuron {
public:
	Neuron(unsigned numOut , unsigned myInd);
	void SetRandWeight(double randVal = 0.0f);
	void SetOutNumbers(unsigned number);
	void SetOutVal(double input);
	double GetOutVal() const { return outVal; }
	void PushForward(const std::vector<Neuron>& prevLayer);
	void SetActivationFunction(double(*fun)( double ));


	void CalcOutGradients(double );
	void CalcHiddenGradients(const std::vector<double>& nextLayer);

	void UpdateInputWeights(std::vector<Neuron>& prevLayer );

private:
	std::vector<Connection> outWeight;
	double outVal;
	unsigned ind;

	double(*activationFunction)( double );
	double(*derivativeFunction)( double );	//pochodna, do wstecznej propagacji

	static double eta;	//[0.0, ... ,1.0]
	static double alpha;// [0.0, ... , n]
};