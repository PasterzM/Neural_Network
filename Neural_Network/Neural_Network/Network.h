#pragma once
#include <vector>
#include <list>


class Neuron;

typedef std::vector<Neuron> Layer;

class Network {
	
public:
	Network();
	Network(const std::vector<unsigned>& netTopology,bool isShowingWork = false);
	
	//zapisanie wasrtosci sieci neuronowej do pliku
	void SaveNeuronsValues();
	
	//przed zaczeciem uczenia sie sieci, za pomoca metody dodajemy warstwy
	bool AddLayer(int numberOfNeutrons);
	
	//na rzecz danych wejsciowych wykonuje proces uczenia sie
	void Learn(const std::vector<double>& vInput);
	
	//wykonanie wstecznej propagacji
	void BackwardPropagation(const std::vector<double>& targetVal);
	
	//zapisanie danych wynikowych po metodzie ucz w zmiennej podanej w argumencie
	void GetResults(std::vector<double>& res) const;

	//dostanie sie do i-tej warstwy w sieci
	Layer& operator[](unsigned it);
private:
	bool isNetworkCreate;
	std::vector<Layer> layers;

	double recentAverageError;
	double recentAverageSmoothThingFactor;
};