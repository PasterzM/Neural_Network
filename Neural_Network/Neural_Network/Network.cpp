#include <iostream>
#include "Network.h"
#include "Neuron.h"


Network::Network() :isNetworkCreate(false) {
}

Network::Network(const std::vector<unsigned>& netTopology , bool isShowingWork) : isNetworkCreate(false) {

	//create every layer and add a 
	for ( unsigned i = 0; i < netTopology.size(); ++i ) {
		if ( isShowingWork )	std::cout << "Add layer " << i << std::endl;
		layers.push_back(Layer());	//Add layer in to network
		unsigned numOfOutputs = ( i == netTopology.size() - 1 ? 0 : netTopology[i + 1] );
		for ( unsigned j = 0; j < netTopology[i]; j++ ) {
			if ( isShowingWork )	std::cout << "\tAdd neuron" << std::endl;
			layers.back().push_back(Neuron(numOfOutputs , i));//Add neuron in to last added layer
		}
	}
	isNetworkCreate = true;
}

void Network::SaveNeuronsValues() {
}

bool Network::AddLayer(int numberOfNeutrons) {
	if ( isNetworkCreate )	return false;
	layers.push_back(Layer());
	for ( signed i = 0; i < numberOfNeutrons; ++i ) {
		layers.back().push_back(Neuron(0 , i));
	}
	//przedostatnia warstwa
	for ( size_t i = 0; i < layers[layers.size() - 2].size(); ++i ) {
		layers[layers.size() - 2][i].SetOutNumbers(numberOfNeutrons);
	}

	return true;
}

void Network::Learn(const std::vector<double>& vInput) {
	if ( layers[0].size() == 0 ) {
		isNetworkCreate = false;	//after execute first time method Input you cant change network
		throw std::exception("There isn't any neurons in the first layer");
	}

	if ( vInput.size() != layers[0].size() )	throw std::exception("Numbers of Input values is not the same as first layer");
	isNetworkCreate = true;

	// Input layer
	for ( unsigned neuron = 0; neuron < vInput.size(); ++neuron ) {
		layers[0][neuron].SetOutVal(vInput[neuron]);
	}

	//hidden layers
	for ( unsigned layer = 1; layer < layers.size(); ++layer ) {
		for ( unsigned neuron = 0; neuron < vInput.size(); ++neuron ) {
			layers[layer][neuron].PushForward(layers[layer - 1]);
		}
	}
}

void Network::BackwardPropagation(const std::vector<double>& targetVal) {
	//RMS
	Layer& outLayer = layers.back();
	double error = 0.0f;

	//last layer
	for ( unsigned i = 0; i < outLayer.size(); ++i ) {
		double dt = targetVal[i] - outLayer[i].GetOutVal();
		error += dt*dt;
	}
	error /= outLayer.size() - 1;
	error = sqrt(error);

	double averageError = ( recentAverageError * recentAverageSmoothThingFactor + error ) / ( recentAverageSmoothThingFactor + 1.0 );

	//last (output) layer calculate gradient
	for ( unsigned i = 0; i < outLayer.size(); ++i )	outLayer[i].CalcOutGradients(targetVal[i]);

	//hidden layers, calculate gradient
	for ( unsigned i = layers.size() - 2; i > 0; --i ) {
		Layer& layer = layers[i];
		Layer& nextLayer = layers[i + 1];

		//for each neuron
		for ( unsigned j = 0; j < layer.size(); ++j ) {
			layer[j].CalcHiddenGradients(nextLayer);
		}
	}

	//update weight
	for ( unsigned i = layers.size() - 1; i > 0; --i ) {
		Layer& prevLayer = layers[i - 1];
		Layer& layer = layers[i];

		//for each neuron
		for ( unsigned j = 0; j < layer.size(); ++j ) {
			layer[j].UpdateInputWeights(prevLayer);
		}
	}
}

void Network::GetResults(std::vector<double>& res) const {
	res.clear();
	for ( unsigned i = 0; i < layers.back().size(); ++i ) {
		res.push_back(layers.back()[i].GetOutVal());
	}
}

Layer& Network::operator[](unsigned it) {
	if ( it < this->layers.size() ) {
		return layers[it];
	}
	return layers[0];
}
