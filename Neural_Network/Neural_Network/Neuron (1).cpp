#include "Neuron.h"

double Neuron::eta = 0.15;
double Neuron::alpha = 0.5;

double ActivFunc(double sum) {
	return tanh(sum);
}
double DerivFunc(double sum) {
	return 1.0 - sum*sum;
}


Neuron::Neuron(unsigned numOut , unsigned myInd) : ind(myInd) , activationFunction(ActivFunc) {
	for ( unsigned i = 0; i < numOut; ++i ) {
		outWeight.push_back(Connection());
		SetRandWeight();
	}
}

void Neuron::SetRandWeight(double randVal) {
	if ( randVal == 0.0f )	outWeight.back().weight = rand() / double(RAND_MAX);
	else outWeight.back().weight = randVal;

}

void Neuron::SetOutNumbers(unsigned number) {
	for ( unsigned i = 0; i < number; i++ ) {
		outWeight.push_back(Connection());
	}
}

void Neuron::SetOutVal(double input) {
	outVal = input;
}

void Neuron::PushForward(const std::vector<Neuron>& prevLayer) {
	double sum = 0.0f;

	for ( unsigned i = 0; i < prevLayer.size(); ++i ) {
		sum += prevLayer[i].GetOutVal() * outWeight[ind].weight;	//suma wazona
	}

	//uzycie funkcji aktywujacej
	outVal = ( *activationFunction )( sum );
}

void Neuron::SetActivationFunction(double(*fun)( double )) {
	activationFunction = fun;
}

void Neuron::CalcOutGradients(double tarVal) {
	//double delta = tarVal - outVal;
	double gradient = ( tarVal - outVal )* derivativeFunction(outVal);

}

void Neuron::CalcHiddenGradients(const std::vector<double>& nextLayer) {
	double sum = 0;

	//sum weight of all output
	for ( unsigned i = 0; i < nextLayer.size(); ++i ) {
		sum += outWeight[i].weight * nextLayer[i];
	}

	double gradient = sum * derivativeFunction(outVal);
}

void Neuron::UpdateInputWeights(std::vector<Neuron>& prevLayer) {
	for ( unsigned i = 0; i < prevLayer.size(); ++i ) {
		Neuron& neu = prevLayer[i];
		double oldDtWeight = neu.outWeight[ind].dtWeight;

		double newDtWeight = eta * outVal * gradient + alpha * oldDtWeight;

		outWeight[ind].dtWeight = newDtWeight;
		outWeight[ind].weight += newDtWeight;
	}
}
