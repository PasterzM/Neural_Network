#pragma once
#include <vector>

class Neuron;
typedef std::vector<Neuron> Layer;

struct Connection {
	double dtWeight;
	double weight;
};

class Neuron {
public:
	Neuron(unsigned numOut , unsigned myInd);
	void SetRandWeight(double randVal = 0.0f);
	void SetOutNumbers(unsigned number);
	void SetOutVal(double input);
	double GetOutVal() const { return outVal; }
	void PushForward(const std::vector<Neuron>& prevLayer);
	void SetActivationFunction(double(*fun)( double ));


	void CalcOutGradients(double );
	
	//obliczenia dla 
	void CalcHiddenGradients(const Layer& nextLayer);

	void UpdateInputWeights(Layer& prevLayer );

private:
	std::vector<Connection> outWeight;
	double outVal;
	unsigned ind;

	double(*activationFunction)( double );	//funkcja aktywacyjna
	double(*derivativeFunction)( double );	//pochodna, do wstecznej propagacji

	static double eta;	//[0.0, ... ,1.0]
	static double alpha;// [0.0, ... , n]

	double gradient;
};

